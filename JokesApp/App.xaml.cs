﻿using System;
using System.IO;
using Plugin.Connectivity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace JokesApp
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());
        }

        static MyJokesDatabase database;
        public static MyJokesDatabase Database
        {
            get
            {
                if(database == null)
                {
                    database = new MyJokesDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "MyJokesSQLite.db3"));
                }
                return database;
            }
        }

        public static bool CheckConnectivity()
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                return false;
            }
            return true;
        }

        protected override void OnStart()
        {
            AppCenter.Start("ios=1a3aa601-dd80-4ff3-bd5b-8dceb85523f3;" + "uwp={Your UWP App secret here};" + "android={Your Android App secret here}", typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
