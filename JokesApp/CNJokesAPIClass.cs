﻿using System;
using System.Collections.Generic;

namespace JokesApp
{

    public class Value
    {
        public int id { get; set; }
        public string joke { get; set; }
        public List<object> categories { get; set; }
    }

    public class CNJokes
    {
        public string type { get; set; }
        public Value value { get; set; }
    }
}
