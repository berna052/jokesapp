﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.AppCenter.Analytics;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace JokesApp
{
    public partial class ChuckNorrisPage : ContentPage
    {
        public ChuckNorrisPage()
        {
            InitializeComponent();

            Title = "Chuck Norris Jokes";

        }

        async void Handle_ShowJoke(object sender, System.EventArgs e)
        {
            if (App.CheckConnectivity() == false)
            {
                await DisplayAlert("No Internet", "No internet connection detected", "Ok");
            }
            else
            {
                Analytics.TrackEvent("ChuckJoke clicked");

                var client = new HttpClient();

                var jokesApiAddress = "http://api.icndb.com/jokes/random";

                var url = new Uri(jokesApiAddress);

                CNJokes JokeData = new CNJokes();

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();

                    JokeData = JsonConvert.DeserializeObject<CNJokes>(jsonContent);
                    var originalQuestion = JokeData.value.joke;


                    var fixingQuestion = originalQuestion.Replace("&quot;", "\"");

                    fixingQuestion = fixingQuestion.Replace("&#039;", "\'");

                    jokeLabel.Text = fixingQuestion;
                }
            }
        }

        async void Handle_SaveJoke(object sender, System.EventArgs e)
        {
            if (jokeLabel.Text != null)
            {
                Analytics.TrackEvent("ChuckSave clicked");

                var joke = new MyJokesModel
                {
                    Joke = jokeLabel.Text,
                    PunchLine = "",
                };
                await App.Database.SaveItemAsync(joke);
            }
        }

    }
}
