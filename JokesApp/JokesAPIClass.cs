﻿using System;
namespace JokesApp
{
    public class JokesAPIClass
    {
        public int id { get; set; }
        public string type { get; set; }
        public string setup { get; set; }
        public string punchline { get; set; }
    }
}
