﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace JokesApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            Title = "MENU";
            //BackgroundImage = "image0.jpg";

            var image2 = new Image
            {
                Source = ImageSource.FromFile("image0.jpg"),
                Aspect = Aspect.AspectFill,
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
        }

        private async void Handler_ChuckNorrisPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ChuckNorrisPage());
        }

        private async void Handler_OtherJokesPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new OtherJokesPage());

        }

        private async void Handler_MyJokes(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new MyJokesPage());
        }

        private async void Handle_OpenTriviaPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new OpenTriviaPage());
        }
    }
}
