﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;

namespace JokesApp
{
    public class MyJokesDatabase
    {
        readonly SQLiteAsyncConnection database;

        public MyJokesDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<MyJokesModel>().Wait();
        }

        public Task<int> SaveItemAsync(MyJokesModel item)
        {
            if (item.ID != 0)
            {
                return database.UpdateAsync(item);
            }
            else
            {
                return database.InsertAsync(item);
            }
        }

        public Task<List<MyJokesModel>> GetAllItems()
        {
            return database.QueryAsync<MyJokesModel>("SELECT * FROM [MyJokesModel]");
        }

        public Task<int> DeleteItem(MyJokesModel joke)
        {
            return database.DeleteAsync(joke);
        }

        public Task<List<MyJokesModel>> DeleteAll()
        {
            return database.QueryAsync<MyJokesModel>("DELETE FROM [MyJokesModel]");
        }
    }
}
