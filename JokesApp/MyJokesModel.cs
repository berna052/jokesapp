﻿using System;
using SQLite;

namespace JokesApp
{
    public class MyJokesModel
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Joke { get; set; }
        public string PunchLine { get; set; }
    }
}
