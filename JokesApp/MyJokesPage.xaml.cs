﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace JokesApp
{
    public partial class MyJokesPage : ContentPage
    {
        public MyJokesPage()
        {
            InitializeComponent();

            Title = "My Jokes";
                
            GetAllJokes();
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            GetAllJokes();
            myJokesList.IsRefreshing = false;
        }

        async void GetAllJokes()
        {
            var allJokes = await App.Database.GetAllItems();

            myJokesList.ItemsSource = null;
            myJokesList.ItemsSource = allJokes;
        }

        void Handle_DeleteJoke(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var joke = (MyJokesModel)menuItem.CommandParameter;
            App.Database.DeleteItem(joke);
            GetAllJokes();
        }

        async void Handle_DeleteAllJokes(object sender, System.EventArgs e)
        {
            var answer = await DisplayAlert("Warning", "Are you sure you want to delete all jokes?", "No", "Yes");
            if (answer == false)
            {
                await App.Database.DeleteAll();
                GetAllJokes();
            }
        }
    }
}
