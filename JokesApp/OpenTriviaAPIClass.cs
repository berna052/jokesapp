﻿using System;
using System.Collections.Generic;

namespace JokesApp
{
    public class Result
    {
        public string category { get; set; }
        public string type { get; set; }
        public string difficulty { get; set; }
        public string question { get; set; }
        public string correct_answer { get; set; }
        public List<string> incorrect_answers { get; set; }
    }

    public class OpenTrivia
    {
        public int response_code { get; set; }
        public List<Result> results { get; set; }
    }
}
