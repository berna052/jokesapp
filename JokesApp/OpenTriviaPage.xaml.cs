﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using Microsoft.AppCenter.Analytics;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace JokesApp
{
    public partial class OpenTriviaPage : ContentPage
    {
        public OpenTriviaPage()
        {
            InitializeComponent();

            Title = "Trivia";
        }

        async void Handle_GetTrivia(object sender, System.EventArgs e)
        {
            if (App.CheckConnectivity() == false)
            {
                await DisplayAlert("No Internet", "No internet connection detected", "Ok");
            }
            else
            {
                Analytics.TrackEvent("GetTrivia clicked");

                var client = new HttpClient();

                var jokesApiAddress = "https://opentdb.com/api.php?amount=1&type=boolean";

                var url = new Uri(jokesApiAddress);

                OpenTrivia openTriviaData = new OpenTrivia();

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();

                    openTriviaData = JsonConvert.DeserializeObject<OpenTrivia>(jsonContent);

                    categoryLabel.Text = openTriviaData.results[0].category;
                    difficultyLabel.Text = openTriviaData.results[0].difficulty;

                    var originalQuestion = openTriviaData.results[0].question;

                    correctAnswer.Text = openTriviaData.results[0].correct_answer;

                    categoryLabel.IsVisible = true;
                    difficultyLabel.IsVisible = true;
                    questionLabel.IsVisible = true;
                    categoryFrame.IsVisible = true;
                    diffiecultyFram.IsVisible = true;

                    trueButton.IsVisible = true;
                    falseButton.IsVisible = true;

                    showResult.Text = "";

                    var fixingQuestion = originalQuestion.Replace("&quot;", "\"");

                    fixingQuestion = fixingQuestion.Replace("&#039;", "\'");

                    questionLabel.Text = fixingQuestion;

                }
            }
        }

        void Handle_CorrectAnswer(object sender, System.EventArgs e)
        {
            var button = (Button)sender;

            if (correctAnswer.Text == button.Text)
            {
                showResult.Text = "Right!";
            }
            else
            {
                showResult.Text = "Wrong!";
            }
        }
    }
}
