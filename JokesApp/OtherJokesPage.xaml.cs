﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using Microsoft.AppCenter.Analytics;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace JokesApp
{
    public partial class OtherJokesPage : ContentPage
    {
        public OtherJokesPage()
        {
            InitializeComponent();

            Title = "Other Jokes";
        }

        async void Handle_ShowJoke(object sender, System.EventArgs e)
        {
            if (App.CheckConnectivity() == false)
            {
                await DisplayAlert("No Internet", "No internet connection detected", "Ok");
            }
            else
            {
                Analytics.TrackEvent("OtherJoke clicked");

                var client = new HttpClient();

                var jokesApiAddress = "https://safe-falls-22549.herokuapp.com/random_joke";

                var url = new Uri(jokesApiAddress);

                JokesAPIClass JokeData = new JokesAPIClass();

                var response = await client.GetAsync(url);

                if (response.IsSuccessStatusCode)
                {
                    var jsonContent = await response.Content.ReadAsStringAsync();

                    JokeData = JsonConvert.DeserializeObject<JokesAPIClass>(jsonContent);

                    var originalQuestion = JokeData.setup;

                    var fixingQuestion = originalQuestion.Replace("&quot;", "\"");
                    fixingQuestion = fixingQuestion.Replace("&#039;", "\'");

                    setupLabel.Text = fixingQuestion;


                    originalQuestion = JokeData.punchline;

                    fixingQuestion = originalQuestion.Replace("&quot;", "\"");
                    fixingQuestion = fixingQuestion.Replace("&#039;", "\'");

                    punchlineLabel.Text = fixingQuestion;

                }
            }
        }

        async void Handle_SaveJoke(object sender, System.EventArgs e)
        {
            if (setupLabel.Text != null)
            {
                Analytics.TrackEvent("OtherSave clicked");

                var joke = new MyJokesModel
                {
                    Joke = setupLabel.Text,
                    PunchLine = punchlineLabel.Text,
                };
                await App.Database.SaveItemAsync(joke);
            }
        }
    }
}
